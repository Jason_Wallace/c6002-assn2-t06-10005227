<span style="color:blue; font-size:22pt"> Individual Report </span>


[//]: <> (introduction)
<span style="color:lightblue; font-size:19pt"> Introduction </span><br>

The purpose of this individual report is to deliver a detail plan of what I have contributed for our group assignments. Guri and I have created detailed wireframes. Guri created the drafts, and I put most of his ideas into draw.io, to represent how our asp.net core website will look like. 

[//]: <> (role allocation)
<span style="color:lightblue; font-size:19pt"> Role Allocation </span><br>
* Draft Wireframes for desktop and mobile - Gurjinder Ghuman
* Draw.io wireframes for desktop and mobile - Myself

[//]: <> (progress)
<span style="color:lightblue; font-size:19pt"> Progress</span><br>

30/08/17 - Groups were decided.

31/08/17 - Slack channel created. Meetings were arranged. I suggested Guri be leader, since he had already suggested the group members, and had made a Trello page for suggestions. I posted a Steam API suggestion to Trello.

5/09/17 - APIs were decided. Guri and I discussed wireframes, and I managed to make 4 desktop wireframes in draw.io

6/09/17 - I created 3 mobile wireframes on draw.io, and I cut desktop wireframes back to 3 (on group request). I convinced team to cut the Facebook API, since Facebook is not used much by the group. 

7/09/17 - I suggested we have at least two APIs, Twitter and Google Maps, which group has agreed to. 